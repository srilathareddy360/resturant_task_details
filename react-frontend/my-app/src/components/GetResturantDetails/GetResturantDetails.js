import { Component } from "react";
import ResturantAllDetails from "../ResturantDetails/details";
import "./GetRestaurantDetails.css";
import { Link } from "react-router-dom";

class AllResturantDetails extends Component {
  state = { ResturantData: [] };
  componentDidMount() {
    this.ApiCall();
  }
  ApiCall = async () => {
    const url = "http://localhost:5000/users/all";
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    console.log(response);
    const data = await response.json();
    console.log(data);
    this.setState({ ResturantData: data });
  };
  render() {
    const { ResturantData } = this.state;

    return (
      <>
      <div className="Get-contianer">
        <div className="d-flex flex-row">
        <h1 className="heading-line mr-5">Resturant Details</h1>
        <div className="d-flex flex-row">
        <Link to='./adddetails'><button className="Add mr-5 " > Create </button></Link>
</div>
        </div>
        <div className="og-row og-li og-li-head">
          <div className="col og-li-col-2 ml-6 details-heading">
            Resturant Name
          </div>
          <div className="col og-li-col-3 text-center ml-6 details-heading">
            Manager Name
          </div>
          <div className="col og-li-col-4 text-center ml-8 details-heading">
            contact Number
          </div>
          <div className="col og-li-col-5 text-center ml-6 details-heading">
            Timings
          </div>
          <div className="col og-li-col-6 text-center ml-6 details-heading">
            Menu
          </div>
         
        
        </div>


        {ResturantData.map((each) => (
          <ResturantAllDetails each={each} key={each.id} />
        ))}
       
     
      </div>
       
    
     
      </>
    );
  }
}

export { AllResturantDetails };
