import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { HomePage } from "./components/Home/Home.js";
import { AddResturantDetails } from "./components/AddResturantDetails/AddResturant.js";
import { DeleteDetails } from "./components/DeleteResturantDetails/DeleteResturantDetails.js";
import {Update} from "./components/UpdateResturatDetails/update.js";
import { NavBar } from "./components/Navbar/Navbar.js";
import { AllResturantDetails } from "./components/GetResturantDetails/GetResturantDetails.js";
import { AddedSuccess } from "./components/sucess/success .js";

function App() {
  return (
    <div className="App">
      <Router>
        <NavBar />
        <Switch>
          <Route exact path="/home">
            <HomePage />
          </Route>

          <Route exact path="/adddetails">
            <AddResturantDetails />
          </Route>
          <Route exact path="/updatedetails" component={Update}/>
            
          <Route exact path="/deletedetails">
            <DeleteDetails />
          </Route>
          <Route exact path="/alldetails">
            <AllResturantDetails />
          </Route>
          <Route path="/addedsuccess" exact component={AddedSuccess} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
