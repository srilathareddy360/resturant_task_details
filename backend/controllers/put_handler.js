//import model
import { ResturantModel } from "../models/model.js";

/**
 * @param update_product_details updates the details of the products
 * @param {*} request express request object
 * @param {*} response express response object
 * update details based on resturant name 
 * timings, manager name ,contact number, menu should update
 * if response is ok data should updated otherwise shows error
 */

const Update_resturant_details = async (request,response)=>{
  const data = request.body
  try {
    const result = await ResturantModel.updateOne(
      { Restaurant_name: data.Restaurant_name },
      {
        $set: {
          Timings: data.Timings,
          Manager_name:data.Manager_name,
          contact_number:data.contact_number,
          Menu:data.Menu


          
        },
      }
      );
      response.send(JSON.stringify({
          status_code: 200,
          status_message: "data updated successfully"
      }))

      console.log(result)
    } catch (err) {
      //catch block catches the exception
      response.send(err);

    }
  // response.send("updates done for the product")

}


//exporting update_product module
export default  Update_resturant_details;