import React, { Component } from "react";

import SearchRestaurants from "../SearchRestaurant/SearchRestaurant.js"


//import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";


import "./Home.css";

class HomePage extends Component {
  state = { 
    search: "",
    ResturantData: [],
    searchFilter: [],
    display: "d-none",
    search_msg:"", }
    ;

    onSubmitSearch= (event)=> {
      event.preventDefault()
      this.componentDidMount()
      this.searchRestaurants()
      
    }

  searchRestaurants = () => {
    let filtered_data = []

    for (const each of this.state.ResturantData) {
      const Restaurant_name = each.Restaurant_name.toLowerCase();
      
        if (Restaurant_name.includes(this.state.search.toLowerCase())) {
          filtered_data.push(each)
        }

    }
    if (filtered_data.length===0){
      this.setState({search_msg:"Resturant Not Found"})
    }
    this.setState({searchFilter:filtered_data, display: "d-block"})

  }


  componentDidMount() {
    this.ApiCall();
  }

  onChangeSearch = (event) => {
    this.setState({ search: event.target.value, search_msg: ""});
  };

  ApiCall = async () => {
    const url = "http://localhost:5000/users/all";
    const option = {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, option);
    const data = await response.json();
    this.setState({ResturantData:data})
  };



  
 
 
  render() {
    const { search, searchFilter, display,search_msg } = this.state;
  

    return (
      <>
        <div className="Home-Container mt-5">
          <center>
            <h1 className="home-heading">Restaurant Details</h1>
          </center>
        <div>
        <div className="form-search mb-5">
          <form className="example" >
            <input type="text" placeholder="Search Resturants..." name="search" onChange={this.onChangeSearch} value={search}/>
            <button type="submit" onClick={this.onSubmitSearch}><i className="fa fa-search"></i></button>
          </form>
        </div>
        <p className="search_msg">{search_msg}</p>
        {searchFilter.length>1?
        <table className={display}>
        <tbody className="heading-details">
          <tr className="heading-details">
            <th className="heading-details"> Restaurant Name</th>
            <th className="heading-details"> Manager Name</th>
            <th className="heading-details"> Contact Number</th>
            <th className="heading-details"> Timings </th>
            <th className="heading-details"> Menu </th>
          </tr>
          {searchFilter.map((eachResturant) => (
            <SearchRestaurants key={eachResturant.id} each={eachResturant} />
          ))}
        </tbody>
      </table>
  :""}
        </div>
        </div>

      </>
    );
  }
}

export { HomePage };
