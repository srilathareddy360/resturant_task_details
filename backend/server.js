// Importing External Dependencies

import express from "express";
import bodyParser from "body-parser";
import mongoose  from "mongoose";
import dotenv from "dotenv";
import cors from "cors";

dotenv.config(); //for reading .env file


//creating an instance of express
const app = express();
// Returns middleware that only parses json
app.use(bodyParser.json()); 
// Retruns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({extended:true}));

app.use(cors());

//internal dependencies
// import router modules
import router from "./Router/router.js"

app.use("/users", router)

//connecting to mongodb
mongoose
    .connect(process.env.MONGO_URL)
    .then(()=> {
        app.listen(process.env.PORT, ()=> {
            console.log("Server Running Sucessfully on PORT " + process.env.PORT)
        })
    })
    .catch((err)=> {
        console.log("There is some issue to connect MongoDB Database." + err)
    })