import React from "react";

import "./UpdateREsturantDetails.css";

class Update extends React.Component {
	state = {
		Restaurant_name: "",
		new_Menu: "",
		new_Timings: "",
    new_Manager_name:"",
    new_contact_number:"",
    error_msg:"unable to update the Restaurant",
    isdata:false
	}
	submitSuccess = () => {
		const { history } = this.props
		history.push("/alldetails")
	}
	apiCallFail = (data) => {
		this.setState({ isdata: true })
	}
	updateApiCall = async (event) => {
		event.preventDefault();
		console.log(this.state);
		const { Restaurant_name, new_Menu, new_Timings,  new_Manager_name,new_contact_number } = this.state
		const url = "http://localhost:5000/users/update"
		const RestaurantDetails = {
			Restaurant_name,
			new_Menu,
			new_Timings,
      new_Manager_name,
      new_contact_number

		}
		const option = {
			method: "PUT",
			body: JSON.stringify(RestaurantDetails),
			headers: {
				"Content-Type": "application/json",
				"Accept": "application/json"
			}
		}
		const response = await fetch(url, option)
    console.log(response)
		const data = await response.json()
		console.log(data);
		if (data.statuscode === 200) {
			this.submitSuccess()
		}
		else {
			this.apiCallFail(data)
		}
	}
	ChangeRestaurantName = (event) => {
		this.setState({ Restaurant_name: event.target.value })

	}

	ChangeNewMenu = (event) => {
		this.setState({ new_Menu: event.target.value })
	}

	ChangeNewTimings = (event) => {
		this.setState({ new_Timings: event.target.value })
	}
	ChangeNewcontactNo = (event) => {
		this.setState({ new_contact_number: event.target.value })
	}
  ChangeNewManagerName = (event) => {
		this.setState({ new_Manager_name: event.target.value })
	}
	render() {
    const { Restaurant_name, Timings, Manager_name, contact_number, Menu,isdata,error_msg } =
      this.state;
		return (

      <>
      <div className="update-Container">
      <h2 class="update-heading">Update Details</h2>
      <center>
        <form onSubmit={this.updateApiCall}>
          <input
            placeholder="Restaurant_name"
            className="input"
            type="text"
            onChange={this.ChangeRestaurantName}
            // onBlur={this.validatename}
            value={Restaurant_name}
            required
          />{" "}

          <br />
          <input
            placeholder="Manager_name"
            className="input"
            type="text"
            onChange={this.ChangeNewManagerName}
            // onBlur={this.validateManagername}
            value={Manager_name}
            required
          />{" "}
          <br />
          <input
            placeholder="contactno"
            className="input"
            type="text"
            onChange={this.ChangeNewcontactNo}
            // onBlur={this.validatecontact_no}
            value={contact_number}
            required
          />{" "}
          <br />
          <input
            placeholder="Menu"
            className="input"
            type="text"
            onChange={this.ChangeNewMenu}
            // onBlur={this.validatemenu}
            value={Menu}
            required
          />{" "}
          <br />
          <input
            placeholder="timings"
            className="input"
            type="text"
            onChange={this.ChangeNewTimings}
            // onBlur={this.validatTimings}

            value={Timings}
            required
          />{" "}
          <br />
          <button type="submit" className="update-button">
            Update
          </button>
          {isdata && <p style={{color:"white"}}>* {error_msg}</p>}
          <div className="link-conteiner mt-5">
          <a href="alldetails" className="success_msg">click here to check details are delete or not</a>
        </div>
        </form>
      </center>
    </div>
    </>
		);
    }
  }
export { Update};