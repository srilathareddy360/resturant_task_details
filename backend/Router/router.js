//importing internal dependencies
import express from "express";

//import external dependenices
//import path handlers
import delete_Resturant_details from "../controllers/delete_handler.js";
import Get_Resturant_details from "../controllers/get_handler.js";
import Restuarant_information_handler from "../controllers/resturant_details_handler.js";
import Update_resturant_details from "../controllers/put_handler.js";



const router = express.Router();


/****************API CALLS******************** */

/**********GET***************** */

router.get("/all", Get_Resturant_details);

/***********POST****************** */
router.post("/create", Restuarant_information_handler);

/*************PUT****************** */
router.put("/update/:id", Update_resturant_details)


/*************DELETE******************/
router.delete('/delete', delete_Resturant_details)


export default router;
