/**
 * This is a helper function to check and validate the mobile_no
 * @param {} mobile_no this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _contactno_validation(contact_number) {
  const returnObject = {};
  if (contact_number === undefined) {
    returnObject.contact = "Mobile Number field is mandatory";
    return returnObject;
  }
  if (typeof contact_number !== "number") {
    returnObject.contact_number = "Mobile Number should be a Number type";
    return returnObject;
  }
  if (!Number.isInteger(contact_number)) {
    returnObject.contact_number =
      "Mobile Number have no special characters points " + contact_number;
    return returnObject;
  }
  if (contact_number.toString().length !== 10) {
    returnObject.contact_number =
      "Mobile number should have 10 digits: " + contact_number;
    return returnObject;
  }

  return returnObject;
}
function _Timings_validation(Timings) {
  const returnObject = {};
  if (Timings === undefined) {
    returnObject.Timings = "timings field is mandatory";
    return returnObject;
  }
  if (typeof Timings !== "number") {
    returnObject.timings = "timings should be a Number type";
    return returnObject;
  }
  if (!Number.isInteger(Timings)) {
    returnObject.timings =
      "timings filed have no special characters points " + Timings;
    return returnObject;
  }
  if (Timings.toString().length >12) {
    returnObject.timings =
      " Timings should have less than 12: " + Timings;
    return returnObject;
  }

  return returnObject;
}

/**
 * This is a helper function to check and validate the firstname
 * @param {} firstname this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _name_validation(Restaurant_name) {
  const returnObject = {};
  // is it a string
  if (Restaurant_name === undefined) {
    returnObject.Restaurant_name = "Please give Your firstname";
    return returnObject;
  }
  if (!(Restaurant_name.match(/^[a-zA-Z ]*$/))){
    returnObject.Restaurant_name = "name field should have only alphabets ";
    return returnObject;
  }
  if (Restaurant_name.length === 0) {
    returnObject.Restaurant_name = "name field is mandatory";
    return returnObject;
  }
  return returnObject;
}

function _managername_validation(Manager_name) {
  const returnObject = {};
  // is it a string
  if (Manager_name === undefined) {
    returnObject.Restaurant_name = "Please give Your mangername";
    return returnObject;
  }
  if (typeof Manager_name !== "string") {
    returnObject.Restaurant_name = "managername should be of string type";
    return returnObject;
  }
  if (Manager_name.length === 0) {
    returnObject.Manager_name = "managername field is mandatory";
    return returnObject;
  }
  return returnObject;
}



/**
 * This is a helper function to check and validate the department
 * @param {} department this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _Menu_validation(Menu) {
  const returnObject = {};
  // is it a string
  if (Menu === undefined) {
    returnObject.Menu = "Please enter menu ";
    return returnObject;
  }
  if (typeof Menu !== "string") {
    returnObject.Menu = "menu should be of string type";
    return returnObject;
  }
  if (Menu.length === 0) {
    returnObject.Menu= "menu field is mandatory";
    return returnObject;
  }
  return returnObject;
}



/**
 * This is a helper function to count the keys in object recieved
 */
function _return_object_keys(obj) {
  let returnArray = [];
  for (let key in obj) {
    returnArray.push(key);
  }

  return returnArray;
}

export {
  _name_validation,
  _contactno_validation,
  _return_object_keys,
  _Menu_validation,
  _Timings_validation,
  _managername_validation
};
