const ALLOWED_HOURS=[1, 2, 3, 4, 5, 6, 7, 8, 9,10,11,12];

/**
 * This is a helper function to check and validate the username
 * @param {} name this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _name_checker(name) {
  const returnObject = {};
  if (name == undefined) {
    returnObject.username = "name field is mandatory";
    return returnObject;
  }
  if (typeof name != "string") {
    returnObject.username = "name must be a string";
    return returnObject;
  }
  if (name.length <= 25 && name.length > 5) {
    returnObject.name = "name length must be in between  25 to 5 characters";
  }
}

/**
 * This is a helper function to check and validate the mobile_no
 * @param {} mobile_no this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _mobile_no_checker(mobile_no) {
  const returnObject = {};
  if (mobile_no == undefined) {
    returnObject.mobile_no = "Contact Number field is Mandatory";
    return returnObject;
  }
  if (typeof mobile_no != "number") {
    returnObject.mobile_no = "Contact Number should be a Number type";
    return returnObject;
  }
  if (!Number.isInteger(mobile_no)) {
    returnObject.mobile_no =
      "Contact Number have no special characters points " + mobile_no;
    return returnObject;
  }
  if (mobile_no.toString().length != 10) {
    returnObject.mobile_no =
      "Contact number should have 10 digits: " + mobile_no;
    return returnObject;
  }

  return returnObject;
}

/**
 * This is a helper function to check and validate the hours
 * @param {} hours this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _Timings_checker(hours) {
  const returnObject = {};
  // is it a string
  if (hours == undefined) {
    returnObject.hours = "Timings is a mandatory parameter";
    return returnObject;
  }
  if (typeof hours != "number") {
    returnObject.hours = "Timings should be of numeric type";
    return returnObject;
  }
  if (!Number.isInteger(hours)) {
    returnObject.hours = "Timings has to be integer type";
    return returnObject;
  }
  if (!ALLOWED_HOURS.includes(hours)) {
    returnObject.hours = "timings can only be only in " + ALLOWED_HOURS;
    return returnObject;
  }

  return returnObject;
}

/**
 * This is a helper function to check and validate the description
 * @param {} description this is the parameter recieved to validate
 * @returns An object containing error messages.
 */
function _Menu_checker(description) {
  const returnObject = {};
  // is it a string
  if (description == undefined) {
    returnObject.description = "Please give Your Menu description";
    return returnObject;
  }
  if (typeof description != "string") {
    returnObject.description = "menu-description should be of string type";
    return returnObject;
  }
  if (description.length == 0) {
    returnObject.description = "menu-description cannot be an empty string";
    return returnObject;
  }
  return returnObject;
}

/**
 * This is a helper function to count the keys in object recieved
 */
function _return_object_keys(obj) {
  let returnArray = [];
  for (let key in obj) {
    returnArray.push(key);
  }

  return returnArray;
}
function Update_Timings(req,res) {
  const { Manager_name, Timings} = req.body;

  BookModel.updateOne(
      { Manager_name: Manager_name },
      { $set: { Timings: Timings } },
      (err, data) => {
        if (err) {
          res.status(406).send("having some issue to update timings");
        } else {
          res.status(200).send("Timings Updated Successfully");
        }
      }
    );
}

export {
  _name_checker,
  _mobile_no_checker,
  _Timings_checker,
  _Menu_checker,
  _return_object_keys,
  Update_Timings
};
