import React, { Component } from "react";


import "./DeleteResturantDetails.css"

class DeleteDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Restaurant_name: "",
      success_msg: "",
      failure_msg: "",
    };
  }

  onChangeName = (event) => {
    this.setState({ Restaurant_name: event.target.value });
  };

  submitForm = async (event) => {
    event.preventDefault();

    this.setState({ success_msg: "" });
    this.setState({ failure_msg: "" });

    const { Restaurant_name } = this.state;

    const Book_Data = { Restaurant_name};
  //taking backend url and method:delete
    const url = "http://localhost:5000/users/delete";

    const options = {
      method: "DELETE",
      body: JSON.stringify(Book_Data),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };

    try {
      const response = await fetch(url, options);

      if (response.status === 200) {
        const msg = "Resturant Deleted Successfully"  
        this.setState({ success_msg: msg });
      } else {
        const msg = "Enter Resturant Name ";
        this.setState({ failure_msg: msg });
      }
    } catch {
      const msg = "unable to connect the server";
      this.setState({ failure_msg: msg });
    }
  };
  render() {
    const { Restaurant_name,success_msg,failure_msg} = this.state;
    return (
      <>
        <div className="d-flex flex-row">
          <div className="main_sec">
          
            <h1 className="main_msg mb-5" style={{ textAlign: "center" ,color:"black"}}> Delete Resturant details </h1>
            <h1 className="succcess_msg mt-4">{success_msg}</h1>
              <h1 className="failure_msg">{failure_msg}</h1>
            <table className="ts_table_delete mt-4 ml-4 d-flex flex-row justify-content-center">
            
              <center>
                <tbody>
                  <tr className="ts_row_delete">
                    <td className="ts_col" style={{backgroundColor:"black"}}>Resturant Name</td>
                    <td className="ts_col">
                      <input
                        type="text"
                        onChange={this.onChangeName}
                        value={Restaurant_name}
                      />
                    </td>
                  </tr>
                </tbody>
              </center>
            </table>
            
            <div className="add_button mt-5">
              <button className="button_style" onClick={this.submitForm}>
                {" "}
                Delete Resturant Details{" "}
              </button>
            </div>
           
            <div className="link-conteiner">
            <a href="alldetails" style={{color:"white"}}>click here to check details are deleted or not</a>
          </div>
          </div>
        </div>
      </>
    );
  }
}

export { DeleteDetails };
