//importing restaurantmodel

import { ResturantModel } from "../models/model.js";

/**

 * this function is used to  gets the all resturant details.

 * @param {*} request express request object

 * @param {*} response express response object

 */

const Get_Resturant_details = (request, response) => {
  //using find function to get the details from model
  ResturantModel.find({}, (error, data) => {
    if (error) {
      console.log(error);
    } else {
      // send the data to the response

      response.send(data);
    }
  });
};

export  default Get_Resturant_details ;
