//import internal dependencies
import React, { Component } from "react";


//import external dependencies
import "./AddResturant.css";
import {
  _name_validation,
  _contactno_validation,
  _return_object_keys,
  _Menu_validation,
  _Timings_validation,
  _managername_validation,
} from "../Validators/Form_validation.js";

class AddResturantDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Restaurant_name: "",
      Manager_name: "",
      contact_number: "",
      Menu: "",
      Timings: "",
      dataMsg: "",
      Restaurant_name_err: "",
      Manager_name_err: "",
      contact_number_err: "",
      Menu_err: "",
      Timings_err: "",
    };
  }
  AdddetailsSuccess = (data) => {
    this.setState({ dataMsg: data.msg });
  };
  apiCallFail = (data) => {
    this.setState({ dataMsg: data.err_msg });
  };

  onChangeResturantName = (event) => {
    this.setState({
      Restaurant_name: event.target.value,
      Restaurant_name_err: "",
    });
  };

  validatename = () => {
    const Restaurant_name = this.state.Restaurant_name;
    const name_errors = _name_validation(Restaurant_name);
    const is_name_validated = _return_object_keys(name_errors).length === 0;
    console.log(name_errors);
    console.log(is_name_validated);
    if (!is_name_validated) {
      // firstname validation failed
      this.setState({ Restaurant_name_err: name_errors.Restaurant_name });
    }
  };

  onChangeMobileNO = (event) => {
    this.setState({
      contact_number: event.target.value,
      contact_number_err: "",
    });
  };

  validatecontact_no = () => {
    const contact_number = this.state.contact_number;
    const phone_number_errors = _contactno_validation(parseInt(contact_number));
    const is_phone_number_validated =
      _return_object_keys(phone_number_errors).length === 0;
    console.log(phone_number_errors);
    console.log(is_phone_number_validated);
    if (!is_phone_number_validated) {
      this.setState({ contact_number_err: phone_number_errors.contact_number });
    }
  };

  onChangeMenu = (event) => {
    this.setState({ Menu: event.target.value, Menu_err: "" });
  };

  validatemenu = () => {
    const Menu = this.state.Menu;
    const menu_errors = _Menu_validation(Menu);
    const is_menu_validated = _return_object_keys(menu_errors).length === 0;
    console.log(menu_errors);
    console.log(is_menu_validated);
    if (!is_menu_validated) {
      this.setState({ Menu_err: menu_errors.Menu });
    }
  };

  onChangeTimings = (event) => {
    this.setState({ Timings: event.target.value, Timings_err: "" });
  };

  validatTimings = () => {
    const timings = this.state.Timings;
    const timings_errors = _Timings_validation(parseInt(timings));
    const is_timings_validated =
      _return_object_keys(timings_errors).length === 0;
    console.log(timings_errors);
    console.log(is_timings_validated);
    if (!is_timings_validated) {
      this.setState({ Timings_err: timings_errors.timings });
    }
  };

  onChangeManagerName = (event) => {
    this.setState({ Manager_name: event.target.value, Manager_name_err: "" });
  };

  validateManagername = () => {
    const Manager_name = this.state.Manager_name;
    const managername_errors = _managername_validation(Manager_name);
    const is_lastname_validated =
      _return_object_keys(managername_errors).length === 0;
    console.log(managername_errors);
    console.log(is_lastname_validated);
    if (!is_lastname_validated) {
      this.setState({ Manager_name_err: managername_errors.Manager_name });
    }
  };

  submitForm = async (event) => {
    event.preventDefault();
    const { Restaurant_name, Manager_name, contact_number, Menu, Timings } =
      this.state;

    const Resturant_details = {
      Restaurant_name,
      Manager_name,
      contact_number: parseInt(contact_number),
      Menu,
      Timings,
    };

    //taking backend url and method :post
    const url = "http://localhost:5000/users/create";

    const options = {
      method: "POST",
      body: JSON.stringify(Resturant_details),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    const response = await fetch(url, options);
    const data = await response.json();
    console.log(data);
    if (data.statusCode === 200) {
      this.setState({ dataMsg: data.msg });
    } else {
      this.apiCallFail(data);
    }
  };

  render() {
    const { name, managername, menu, Timings, contact, dataMsg } = this.state;
    return (
      <>
        <div>
          <div className="main_sec">
            <h1
              className="main_msg mr-2"
              style={{ textAlign: "center", color: "black" }}
            >
              {" "}
              Add Restaurant Details{" "}
            </h1>

            <h1 className="success_msg"> {dataMsg}</h1>
            
            <table className="ts_table d-flex flex-row justify-content-center mr-8 mt-5">
              <tbody>
                <tr className="ts_row">
                  <td className="ts_col">Restaurant Name</td>
                  <td className="ts_col">
                    <input
                      type="text"
                      onChange={this.onChangeResturantName}
                      onBlur={this.validatename}
                      value={name}
                      required
                    />
                    <p style={{ color: "red" }}>
                      {this.state.Restaurant_name_err}
                    </p>
                  </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col">Manager Name</td>
                  <td className="ts_col">
                    <input
                      type="text"
                      onChange={this.onChangeManagerName}
                      onBlur={this.validatename}
                      value={managername}
                      required
                    />
                    <p style={{ color: "red" }}>
                      {this.state.Restaurant_name_err}
                    </p>
                  </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col">Contact No</td>
                  <td className="ts_col">
                    <input
                      type="text"
                      onChange={this.onChangeMobileNO}
                      onBlur={this.validatecontact_no}
                      value={contact}
                      required
                    />
                    <p style={{ color: "red" }}>
                      {this.state.contact_number_err}
                    </p>
                  </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col">Timings</td>
                  <td className="ts_col">
                   
                    <input
                      type="text"
                      onChange={this.onChangeTimings}
                      onBlur={this.validatTimings}
                      value={Timings}
                      required
                    />
                    <p style={{ color: "red" }}>{this.state.Timings_err}</p>
                  </td>
                </tr>
                <tr className="ts_row">
                  <td className="ts_col">Menu</td>
                  <td className="ts_col">
                    <input
                      type="text"
                      onChange={this.onChangeMenu}
                      onBlur={this.validatemenu}
                      value={menu}
                      required
                    />
                    <p style={{ color: "red" }}>{this.state.Menu_err}</p>
                  </td>
                </tr>
              </tbody>
            </table>
            <div className="add_button">
              <button className="button_style" onClick={this.submitForm}>
                {" "}
                Add Resturant Details{" "}
              </button>
            </div>
            <div className="link-conteiner">
              <a href="alldetails" style={{ color: "white" }}>
                click here to check details are added or not
              </a>
            </div>
          </div>
        </div>
      </>
    );
  }
}

//exporting Addrestaurantdetails module
export { AddResturantDetails };
