import {  _name_checker, _return_object_keys } from "./helper_functions.js";

/**
 * This is the delete_validator to validate the QUERY params 
 * sent in this DELETE call
 */
const Delete_validator = (req, res, next) => {

  const { Restaurant_name } = req.params; 
	//const emp_id_as_number = parseInt(Restaurant_name;
  // check if everything is okay with all params

		const error_object = { ... _name_checker(Restaurant_name)};

		if (_return_object_keys(error_object).length > 0) {
			res.status(400).send({err: error_object});
		} else {
			next();
		}
	
};
export { Delete_validator };
