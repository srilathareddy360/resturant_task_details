// import mongoose
import mongoose from "mongoose";

/**
 * Schema for creating UserModel
 */
const RestuarantSchema = new mongoose.Schema({
  Restaurant_name: {type:String, required:true, unique:true},
  Manager_name: {type:String, required:true},
  contact_number: {type:Number, required:true},
  Menu: {type:String, required:true},
  Timings: {type:Number, required:true},
   
})

//mongoose model
const ResturantModel = mongoose.model("ResturantModel",RestuarantSchema);

export {ResturantModel};

