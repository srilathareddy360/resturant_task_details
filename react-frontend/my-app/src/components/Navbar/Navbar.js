import React from "react";
import { Navbar, Container, Nav } from "react-bootstrap";

import "./Navbar.css";

function NavBar() {
  return (
    <>
       <Navbar bg="dark" expand="sm" variant="">
                <div>
                    <img src="https://s.tmimgcdn.com/scr/800x500/181400/food-restaurant-logo-design-template_181494-original.jpg" style={{ height: "60px", width: "90px" }} alt="" />
                </div>
                <Container className="colors">

                    <Navbar.Brand href="home">Restaurant </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="/home">Home</Nav.Link>
                            <Nav.Link href="/adddetails">Add Restaurant Details</Nav.Link>
                            <Nav.Link href="/alldetails">All Restuarant Details</Nav.Link>
                           
                            
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
    </>
  );
}

export   {NavBar};
