//import ResturantModel 
import { ResturantModel } from "../models/model.js";


/**
 * This hanlder function is used for getting all the books that are available in database
 * @param {*} requests express request object
 * @param {*} response express response object
 * delete details based on resturant name
 */
 const delete_Resturant_details = ((req,res)=> {

    const {Restaurant_name} = req.body;

    if (Restaurant_name.length !== 0) {
  //delet details using deleteOne function from model      
        ResturantModel.deleteOne({Restaurant_name:Restaurant_name},(err,data)=> {
            if (err) {
                res.status(404).send("Resturant details not deleted from database having some issue")
            }
            else {
                //if deletedCount is zero then resturant is Not available or already deleted 
                if (data.deletedCount === 0) {
                    res.status(404).send("already deleted")
                }
              
                else {
                res.status(200).send(Restaurant_name+" deleted Successfully")
                }
            }
        })
    }
    else {
        res.status(406).send("details should not be empty")
    }
})
//export delete_Resturant_details
export  default delete_Resturant_details