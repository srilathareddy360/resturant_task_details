import React from "react";
// import { Navbar } from "react-bootstrap";
import "./UpdateREsturantDetails.css";



//import { Alert } from "react-alert";
// import {Link} from 'react-router-dom'
// import {}
import {
  _name_validation,
  _contactno_validation,
  _return_object_keys,
  _Menu_validation,
  _Timings_validation,
  _managername_validation,
} from "../Validators/Form_validation.js";


class UpdateResturantDetails extends React.Component {
  state = {
    Restaurant_name: "",
    Restaurant_name_err: "",

    new_Manager_name:"",
    new_Manager_name_err: "",

    new_Timings:"",
    new_Timings_err: "",

    new_Menu:"",
    new_Menu_err:"",

    new_contact_number:"",
    new_contact_number_err:"",

    Restaurant_name_db_err: "",
    dataMsg: ""
  };

  updatedetailsSuccess = (data) => {
    this.setState({ dataMsg: data.msg });
  };

  apiCallFail = (data) => {
    this.setState({ erro_msg: data.msg });
  };
 
  updateApiCall = async (event) => {
    event.preventDefault();
    console.log("STATE",this.state);
    const { Restaurant_name, new_Manager_name ,new_contact_number, new_Menu, new_Timings } = this.state;
    const url = "http://localhost:5000/users/update";
    const ResturantDetails = {
      Restaurant_name,
      new_Manager_name,
      new_contact_number,
      new_Menu,
      new_Timings 
    };
    console.log("BODY",ResturantDetails)
    const option = {
      method: "PUT",
      body: JSON.stringify(ResturantDetails),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    
    const response = await fetch(url, option);
    const data = await response.json();
    console.log(data);
    if (data.status_code === 200) {
      console.log(data);
      this.UpdateSuccess();
    } else {
      this.apiCallFail(data);
    }
  };

  
  onChangeResturantName = (event) => {
    this.setState({
      Restaurant_name: event.target.value,
      Restaurant_name_err: "",
    });
  };

  validatename = () => {
    const Restaurant_name = this.state.Restaurant_name;
    const name_errors = _name_validation(Restaurant_name);
    const is_name_validated = _return_object_keys(name_errors).length === 0;
    console.log(name_errors);
    console.log(is_name_validated);
    if (!is_name_validated) {
      // firstname validation failed
      this.setState({ Restaurant_name_err: name_errors.Restaurant_name });
    }
  };

  onChangeMobileNO = (event) => {
    this.setState({
      contact_number: event.target.value,
      contact_number_err: "",
    });
  };

  validatecontact_no = () => {
    const contact_number = this.state.contact_number;
    const phone_number_errors = _contactno_validation(parseInt(contact_number));
    const is_phone_number_validated =
      _return_object_keys(phone_number_errors).length === 0;
    console.log(phone_number_errors);
    console.log(is_phone_number_validated);
    if (!is_phone_number_validated) {
      this.setState({ contact_number_err: phone_number_errors.contact_number });
    }
  };

  onChangeMenu = (event) => {
    this.setState({ Menu: event.target.value, Menu_err: "" });
  };

  validatemenu = () => {
    const Menu = this.state.Menu;
    const menu_errors = _Menu_validation(Menu);
    const is_menu_validated = _return_object_keys(menu_errors).length === 0;
    console.log(menu_errors);
    console.log(is_menu_validated);
    if (!is_menu_validated) {
      this.setState({ Menu_err: menu_errors.Menu });
    }
  };

  onChangeTimings = (event) => {
    this.setState({ Timings: event.target.value, Timings_err: "" });
  };

  validatTimings = () => {
    const timings = this.state.Timings;
    const timings_errors = _Timings_validation(parseInt(timings));
    const is_timings_validated =
      _return_object_keys(timings_errors).length === 0;
    console.log(timings_errors);
    console.log(is_timings_validated);
    if (!is_timings_validated) {
      this.setState({ Timings_err: timings_errors.timings });
    }
  };

  onChangeManagerName = (event) => {
    this.setState({ Manager_name: event.target.value, Manager_name_err: "" });
  };

  validateManagername = () => {
    const Manager_name = this.state.Manager_name;
    const managername_errors = _managername_validation(Manager_name);
    const is_lastname_validated =
      _return_object_keys(managername_errors).length === 0;
    console.log(managername_errors);
    console.log(is_lastname_validated);
    if (!is_lastname_validated) {
      this.setState({ Manager_name_err: managername_errors.Manager_name });
    }
  };

  render() {
    const { Restaurant_name, Timings, Manager_name, contact_number, Menu } =
      this.state;

    return (
      <>
        <div className="update-Container">
        <h2 class="update-heading">Update Details</h2>
        <center>
          <form onSubmit={this.updateApiCall}>
            <input
              placeholder="Restaurant_name"
              className="input"
              type="text"
              onChange={this.onChangeResturantName}
              onBlur={this.validatename}
              value={Restaurant_name}
              required
            />{" "}

            <br />
            <input
              placeholder="Manager_name"
              className="input"
              type="text"
              onChange={this.onChangeManagerName}
              onBlur={this.validateManagername}
              value={Manager_name}
              required
            />{" "}
            <br />
            <input
              placeholder="contactno"
              className="input"
              type="text"
              onChange={this.onChangeMobileNO}
              onBlur={this.validatecontact_no}
              value={contact_number}
              required
            />{" "}
            <br />
            <input
              placeholder="Menu"
              className="input"
              type="text"
              onChange={this.onChangeMenu}
              onBlur={this.validatemenu}
              value={Menu}
              required
            />{" "}
            <br />
            <input
              placeholder="timings"
              className="input"
              type="text"
              onChange={this.onChangeTimings}
              onBlur={this.validatTimings}

              value={Timings}
              required
            />{" "}
            <br />
            <button type="submit" className="update-button">
              Update
            </button>
            <div className="link-conteiner mt-5">
            <a href="alldetails" className="success_msg">click here to check details are delete or not</a>
          </div>
          </form>
        </center>
      </div>
      </>
    );
  }
}


export {UpdateResturantDetails};
