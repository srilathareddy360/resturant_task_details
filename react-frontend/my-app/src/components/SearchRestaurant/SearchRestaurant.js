import "./SearchRestaurant.css";

const SearchRestaurants = (props) => {
  const { each } = props;
  return (
    
    <tr className="mt-4">
      <th className="Details">{each.Restaurant_name}</th>
      <th className="Details"> {each.Manager_name}</th>
      <th className="Details"> {each.contact_number}</th>
      <th className="Details"> {each.Timings}</th>
      <th className="Details"> {each.Menu}</th>
    </tr>
    
  );
};

export default SearchRestaurants;






